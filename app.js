/* =====================================
 ====== Include all needed modules =====
 ===================================== */
 console.log('Starting app');

 require('dotenv').config()
 
 process.on('unhandledRejection', (reason, p) => {
     console.log(p, reason);  
 });
  
 process.on('uncaughtException', (err) => {
     console.log('UncaughtException', err);
 }); 
 
 process.on('warning', (warning) => {
     console.warn(warning.name);    // Print the warning name
     console.warn(warning.message); // Print the warning message
     console.warn(warning.stack);   // Print the stack trace
 });
 
 console.table(process.versions);
 
 const http = require('http');
 const express = require('express');
 const app = express();
 
 const compression = require('compression');
 const bodyParser = require('body-parser');
 const CONSTANTS = require('./constants/constants');
 const locals = require('./lib/Locals');
 const {xPoweredBy} = require('./lib/middleware');
 const cors = require('cors');

 app.use(express.json({limit: '50mb'}));
 app.use(express.urlencoded({limit: '50mb', extended: true}));
 
 /* ===================================== 
  =========== Local functions ===========  
  ===================================== */
 function start(app) {
     startServer(app, process.env.PORT || 8080);
 }
  
 async function appInit(app) {
     const {join} = require('path');
     const {promisify} = require('util');
     const readDir = promisify(require('fs').readdir);
 
     const appPath = join(__dirname, '/app');
 
     const appComponents = await readDir(appPath);
 
     for (let componentName of appComponents) {
         try {
             const component = require(`${appPath}/${componentName}`);
             console.log('Loading component', component.modelName);
             if (component.routes)
                 component.routes(app);
         } catch (e) {
             console.log('error', e);
         }
     }
 }
 
 function startServer(app, port = 3000) {
     http.createServer(app).listen(port, () => {
         console.log('Server started');
         console.log(`Listening on port ${port}`);
     })
 }
 
 /* =====================================
  ======== Express Pre-Middleware =======
  ===================================== */
 if (process.env.NODE_ENV === 'development') {
     app.use(cors({
         origin: ['http://localhost:8080', 'https://opgewektportaal.nl'],
         methods: 'GET,HEAD,PUT,POST,DELETE',
         credentials: true
     }));
 }else{
    app.use(cors({
        origin: ['http://localhost:8080', 'https://opgewektportaal.nl'],
        methods: 'GET,HEAD,PUT,POST,DELETE',
        credentials: true
    }));
 }
 
 app.set(CONSTANTS.DOMAIN, `${process.env.APP_URL}`);
 app.set(CONSTANTS.SSL, true);
 if (process.env.NODE_ENV === 'development') {
     app.set(CONSTANTS.DOMAIN, 'localhost:8080');
     app.set(CONSTANTS.SSL, false);
 }
 
 
 /* =====================================
  ======== Express Pre-Middleware =======
  ===================================== */
 app.use(compression());
 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded({extended: true}));
 app.use(xPoweredBy);
 
 app.use('/uploads', express.static('./uploads'));
 
 app.use(locals);
 
 app.use((req, res, next) => {
     res.locals.reqTime = (new Date()).getTime();
     console.log(`${req.method} ${req.originalUrl}`);
     next();
 });
 
 /* =====================================
  =========== Init application ==========
  ===================================== */
 appInit(app)
     .then(() => {
         const ResponseRouter = require('./lib/ResponseRouter');
         app.use(ResponseRouter.sendAPI);
         app.use(ResponseRouter.sendError);
 
         /* =====================================
          ============= Start Server ============
          ===================================== */
         start(app);
     });
 