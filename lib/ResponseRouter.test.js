describe('ResponseRouter', () => {
    const ResponseRouter = require('./ResponseRouter');
    const Locals = require('./Locals').Locals;
    let req, res, next;

    beforeEach(() => {
        req = {};
        res = {
            json: jest.fn(),
            status: jest.fn(),
            locals: new Locals(req)
        };
        next = jest.fn();
    });

    describe('.sendAPI', () => {
        test('Should set an error and call next if no body is set', () => {
            ResponseRouter.sendAPI(req, res, next);

            expect(res.locals.body).toHaveProperty('error', 'Route not found');
            expect(res.locals.body).toHaveProperty('status', 404);
            expect(res.json).toHaveBeenCalledTimes(0);
            expect(next).toHaveBeenCalledTimes(1);
        });

        test('Should call res.json when a body is set', () => {
            res.locals.setBody({api: 'data'});

            ResponseRouter.sendAPI(req, res, next);

            expect(res.json).toHaveBeenCalledTimes(1);
            expect(next).toHaveBeenCalledTimes(0);
            expect(res.json).toBeCalledWith(res.locals);
        })

    });

    describe('.sendError', () => {
        test('Should call res.status and res.json with the appropriate data', () => {
            ResponseRouter.sendAPI(req, res, () => {
                ResponseRouter.sendError(req, res);

                expect(res.status).toHaveBeenCalledTimes(1);
                expect(res.status).toBeCalledWith(404);
                expect(res.json).toHaveBeenCalledTimes(1);
                expect(res.json).toBeCalledWith(res.locals);
            });
        });
    });
});
