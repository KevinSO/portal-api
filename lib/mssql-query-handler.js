const sql = require("mssql");
const SINGLEROW = 'SINGLEROW';
const MULTIPLEROW = 'MULTIPLEROW';
const NORESPONSE = 'NORESPONSE';

connectionConfig = {
	user: process.env.DBUSER,
	password: process.env.DBPASS,
	server: process.env.DBSERV, 
	database: process.env.DBNAME,
	encrypt: true,
    pool: {
        idleTimeoutMillis: 60000
	},
	requestTimeout: 120000
};
function recordsetToArray (recordset){
	return Object.keys(recordset).map(key => {
		return recordset[key];
	})
}
function handleResult(result, resultType) {
	if(resultType === NORESPONSE)
	return null;
	if(resultType === SINGLEROW)
		return result.recordset.length >= 1 ? result.recordset[0] : recordsetToArray(result.recordset);
	else
		return recordsetToArray(result.recordset);
}
query = async (query, resultType = SINGLEROW) => {
	const pool = await new sql.ConnectionPool(connectionConfig).connect();
	const result = await pool.request().query(query);
	return handleResult(result, resultType);
}
queryWithParams = async (query, params = [], resultType = SINGLEROW) => {
	const pool = await new sql.ConnectionPool(connectionConfig).connect();
	const request = pool.request();
	
	params.forEach(param => {
		request.input(param.name, param.type, param.value);
	})
	
	const result = await request.query(query);
	return handleResult(result, resultType);
}


module.exports = {
  query,
  queryWithParams,
  SINGLEROW,
  MULTIPLEROW,
  NORESPONSE,
  types : {
	VarChar: sql.VarChar,
	Int: sql.Int,
	DateTime: sql.DateTime
  }
};