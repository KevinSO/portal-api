const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');
const fileDir = __dirname;
const {promisify} = require('util');
const readFileAsync = promisify(fs.readFile);
const existsAsync = promisify(fs.exists);


const sendEmail = async ({emailTo, nameTo, subject, content}) => {

    let transporter = nodemailer.createTransport(
        {
            host: 'smtp.office365.com',
            port: 25,
            secure: false,
            auth: {
                user: 'systeem@slimopgewekt.nl',
                pass: 'v6L40qFqIka$'
            },
            logger: true,
            debug: true
        },
        {
            from: 'Opgewekt Portaal <portaal@slimopgewekt.nl>',
            headers: {
                'X-Generated-By': 'transip-vps2/portal-api'
            }
        }
    );
    let message = {
        to: `${nameTo} <${emailTo}>`,
        bcc: `Kevin <Kevin@slimopgewekt.nl>`,
        subject: `${subject}`,
        html: content,
    };
    await transporter.sendMail(message);
    transporter.close(); 
}

const buildTemplate = async (template, data) => {
    const templatepath = '../templates/email';
    const filePath = path.join(fileDir, templatepath, `${template}.html`);
    console.log('filePath', filePath)
    const exists = await existsAsync(filePath);
    console.log('exists', exists)
    if (exists) {
        let fileContent = (await readFileAsync(filePath)).toString();
        Object.keys(data).forEach(key => {
            const value = data[key];
            fileContent = fileContent.replace(`#{${key}}`, value);
        })
        return fileContent;
    } else{
        return JSON.stringify(data);
    }
}

module.exports = {
    sendEmail: sendEmail,
    buildTemplate: buildTemplate
}