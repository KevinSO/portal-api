const xPoweredBy = (_, res, next) => {
    res.header('X-Powered-By', 'Slim Opgewekt');
    next();
};

const skipRouteWhenContentExists = (_, res, next) => {
    if (res.locals.bodyIsSet())
        return next('route');
    next();
};

module.exports = {
    xPoweredBy,
    skipRouteWhenContentExists
};
