
const moment = require('moment');
const _ = require('lodash');

const mergeInverterData = (data, isYear, keys = _.range(2012,new Date().getFullYear() + 1)) => {
    const defaultSet = {};
    console.log('keys', keys)
    keys.forEach(key => {
        if(!defaultSet[key])
        defaultSet[key] = 0
    })
    const dataSet = {};
    data.forEach(row => {
        if(!dataSet[row.InverterId])
            dataSet[row.InverterId] = _.clone(defaultSet);
        const time = moment(utcDateString(row.Time)).utc().format(isYear ? 'YYYY' : 'YYYY-MM');
        dataSet[row.InverterId][time] = row.Etotal;
    });

    Object.keys(dataSet).forEach(inv => {
        let lastVal = 0;
        Object.keys(dataSet[inv]).forEach(key => {
            if(dataSet[inv][key] === 0)
                dataSet[inv][key] = lastVal;
            lastVal = dataSet[inv][key];
        });
    })

    const resultSet = _.clone(defaultSet);
    Object.keys(dataSet).forEach(inv => {
        Object.keys(dataSet[inv]).forEach(key => {
            resultSet[key] += dataSet[inv][key];
        });
    }) 
    return resultSet;
}
const calculateIntervalsYield = (data, minDate, isYears) => {
    let lasttotal = 0;
    const results = [];
    console.log(data);
    Object.keys(data).forEach(key => {
        const value = data[key];
        if(((!isYears || value > 0) && moment(key) >= minDate) ){
            const periodTotal = _.round(value - lasttotal, 1);
            results.push({date: key, yield: periodTotal});
        }
        if(value != 0)
            lasttotal = value;
    })

    return results;
}
const utcDateString = (dateObj) => {
    return `${dateObj.getUTCFullYear()}-${dateObj.getUTCMonth() < 9 ? '0' : ''}${dateObj.getUTCMonth() + 1}-${dateObj.getUTCDate() < 10 ? '0' : ''}${dateObj.getUTCDate()}`;
}
const completeDate = (str) => {
    if(str.length === 4)
        str += "-01";
    if(str.length === 7)
        str += "-01";
    return str;
}
const groupConsumptionData = (data, minDate, dateFormat, isYears) => {
    const locations = {};
    data.forEach(row => {
        if(!locations[row.LocationId])
            locations[row.LocationId] = [];
        locations[row.LocationId].push(row);
    });
    const mappedLocations = Object.keys(locations).map(loc => {
       return calculateIntervalsConsumption(locations[loc], minDate, dateFormat, isYears)
    });
    const tmpResults = [];
    mappedLocations.forEach(locData => {
        locData.forEach(row => {
            if(!tmpResults[row.date])
                tmpResults[row.date] = {
                    usage: 0,
                    return: 0
                };
            tmpResults[row.date].usage += row.usage;
            tmpResults[row.date].return += row.return;
        })
    });
    const keys = Object.keys(tmpResults);
    keys.sort();
    return keys.map(key => {
       return {
           date: key,
           usage: _.round(tmpResults[key].usage, 1),
           return: _.round(tmpResults[key].return, 1)
       }
    });
}
const calculateIntervalsConsumption = (data, minDate, dateFormat, isYears) => {

    let inVal = 0;
    let inHighVal = 0;
    let inLowVal = 0;
    
    let outVal = 0;
    let outHighVal = 0;
    let outLowVal = 0;
    
    let inHighOffset = 0;
    let inLowOffset = 0;
    let outHighOffset = 0;
    let outLowOffset = 0;

    const results = [];
    data.forEach(row => {
        const time = moment(utcDateString(row.Time));
        if(time >= minDate){
            if(row.EtotalInHigh < inHighVal)
                inHighOffset = inHighVal;
            if(row.EtotalInLow < inLowVal)
                inLowOffset = inLowVal;
            if(row.EtotalOutHigh < outHighVal)
                outHighOffset = outHighVal;
            if(row.EtotalOutLow < outLowVal)
                outLowOffset = outLowVal;
                
            const periodUsage = _.round((row.EtotalInHigh + row.EtotalInLow) + (inHighOffset + inLowOffset) - (inHighVal + inLowVal), 1);
            const periodReturn = _.round((row.EtotalOutHigh + row.EtotalOutLow) + (outHighOffset + outLowOffset) - (outHighVal + outLowVal), 1);
            results.push({date: time.format(dateFormat) , usage: periodUsage, return: periodReturn});
        }
        inHighVal = row.EtotalInHigh;
        inLowVal = row.EtotalInLow;
        outHighVal = row.EtotalOutHigh;
        outLowVal = row.EtotalOutLow;
    })

    return results;
}

module.exports = {
    mergeInverterData,
    calculateIntervalsYield,
    calculateIntervalsConsumption,
    groupConsumptionData
}