describe('middleware', () => {
    const middleware = require('./middleware');
    let req, res, next;

    beforeEach(() => {
        req = {};
        res = {
            header: jest.fn()
        };
        next = jest.fn();
    });

    describe('xPoweredBy', () => {
        test('Should set header "X-Powered-By" to "Slim Opgewekt"', () => {
            middleware.xPoweredBy(req, res, next);

            expect(res.header).toHaveBeenCalledTimes(1);
            expect(res.header).toBeCalledWith('X-Powered-By', 'Slim Opgewekt');
        });

        test('Should call next', () => {
            middleware.xPoweredBy(req, res, next);

            expect(next).toHaveBeenCalledTimes(1);
        });
    });

    describe('.skipRouteWhenContentExists', () => {

        beforeEach(() => {
            res.locals = {};
        });

        test('Should call "next" with param "route" when body exists', () => {
            res.locals.bodyIsSet = () => true;

            middleware.skipRouteWhenContentExists(req, res, next);

            expect(next).toHaveBeenCalledTimes(1);
            expect(next).toHaveBeenCalledWith('route');
        });

        test('Should call "next" without param "route" when body doesn\'t exist', () => {
            res.locals.bodyIsSet = () => false;

            middleware.skipRouteWhenContentExists(req, res, next);

            expect(next).toHaveBeenCalledTimes(1);
            expect(next).not.toHaveBeenCalledWith('route');
        });


    });
});
