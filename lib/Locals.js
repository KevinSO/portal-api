const moment = require('moment');

class Locals {

    constructor(req) {
        this.body = {};
        this.query = req.query;

        this.NODE_ENV = process.env.NODE_ENV || 'development';

        this.moment = moment;
    }

    env(envCheck = 'production') {
        return this.NODE_ENV === envCheck
    }

    setBody(body, overwrite = false) {
        if (!overwrite && this.bodyIsSet(body))
            return;
        Object.assign(this.body, body);
    }

    bodyIsSet(body = null) {
        if (!body) {
            return Object.keys(this.body).length > 0;
        }
        const key = Object.keys(body)[0];
        return this.body.hasOwnProperty(key);
    }

    toJSON() {
        return this.body;
    }
}

module.exports = (req, res, next) => {
    res.locals = new Locals(req);
    next();
};

module.exports.Locals = Locals;
