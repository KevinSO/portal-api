class ResponseRouter {

    static sendAPI(req, res, next) {
        console.log(`request took: ${(new Date()).getTime() - res.locals.reqTime}ms`);
        if(!res.locals.bodyIsSet()){
            res.locals.setBody({error: 'Route not found'});
            res.locals.setBody({status: 404});
            return next();
        }
        res.json(res.locals);
    }

    static sendError(error, req, res, next) {
        console.log(error)
        res.status(res.locals.body.status || 500);
        res.json(res.locals);
    }

}

module.exports = ResponseRouter;
