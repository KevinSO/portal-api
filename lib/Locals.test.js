describe('Locals', () => {
    const localsModule = require('./Locals');
    const Locals = localsModule.Locals;
    let local, req;

    beforeEach(() => {
        req = {
            query: {}
        };

        local = new Locals(req);
    });

    describe('Locals', () => {
        describe('instance', () => {
            test('Should set the body to an empty object', () => {
                expect(local).toHaveProperty('body', {});
            });

            test('Should set the query equal to req.query', () => {
                req.query.id = 'abcdef123456';
                const locals = new Locals(req);

                expect(locals.query).toHaveProperty('id', req.query.id);
            });

            test('Should set the NODE_ENV to process.env.NODE_ENV', () => {
                expect(local).toHaveProperty('NODE_ENV', process.env.NODE_ENV);
            });

            test('Should set moment', () => {
                expect(local).toHaveProperty('moment', require('moment'));
            });
        });

        describe('.env', () => {
            test('Should return a boolean', () => {
                expect(typeof local.env() === 'boolean').toBeTruthy();
            });

            test('Should return true if the current process env is the same', () => {
                expect(local.env(process.env.NODE_ENV)).toBeTruthy();
            });

            test('Should return false if the current process env is not the same', () => {
                expect(local.env('SomethingFalsy')).toBeFalsy();
            })
        });

        describe('.setBody', () => {
            test('Should add the object to local.body', () => {
                const key = 'foo';
                const obj = {
                    [key]: 'value'
                };

                local.setBody(obj);

                expect(local.body).toHaveProperty(key, obj[key]);
            });

            test('Should not add the object to local.body if the key already exists', () => {
                const key = 'foo';
                const originalValue = 'value';
                const obj = {
                    [key]: originalValue
                };

                local.setBody(obj);
                obj[key] = 'wrong value';
                local.setBody(obj);

                expect(local.body).toHaveProperty(key, originalValue);
            });
        });

        describe('.bodyIsSet', () => {
            test('Should return true if there are keys and no body is given', () => {
                const key = 'foo';
                const obj = {
                    [key]: 'value'
                };
                local.body = {...local.body, ...obj};

                expect(local.bodyIsSet()).toBeTruthy();
            });
            test('Should return false if there are no keys and no body is given', () => {
                expect(local.bodyIsSet()).toBeFalsy();
            });

            test('Should return false if the key is not present', () => {
                const key = 'foo';
                const obj = {
                    [key]: 'value'
                };

                expect(local.bodyIsSet(obj)).toBeFalsy();
            });

            test('Should return true if the key is present', () => {
                const key = 'foo';
                const obj = {
                    [key]: 'value'
                };

                local.body = {...local.body, ...obj};

                expect(local.bodyIsSet(obj)).toBeTruthy();
            });
        });

        describe('.toJSON', () => {
            test('Should return a JSON object', () => {
                const result = local.toJSON();

                expect(result instanceof Locals).toBeFalsy();
            });

            test('Should be the locals body', () => {
                const key = 'foo';
                const obj = {
                    [key]: 'value'
                };
                local.body = {...local.body, ...obj};

                const result = local.toJSON();

                expect(result).toHaveProperty(key, obj[key]);
            });
        });
    });

    describe('Middleware', () => {

        let res;

        beforeEach(() => {
            res = {
                locals: null
            };
        });

        test('Should set a new instance of Locals on res.locals', () => {
            localsModule(req, res, () => {
                expect(res.locals instanceof Locals).toBeTruthy();
            });
        });
    });
});
