const fs = require('fs');
const {join} = require('path');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const uuidv4 = require('uuid/v4');

const hash = (password, salt) => {
    const hash = crypto.pbkdf2Sync(Buffer.from(password, 'utf8'), Buffer.from(salt, 'utf8'), 100000, 64, 'sha512');
    return hash.toString('base64');
}

const timeConsistentEquals = (a, b) => {
    const bufferA = Buffer.from(a);
    const bufferB = Buffer.from(b);
    
    try
    {
        let diff = bufferA.length ^ bufferB.length;
        for (let i = 0; i < bufferA.length && i < bufferB.length; i++){
            diff |= bufferA.readUIntBE(i, 1) ^ bufferB.readUIntBE(i, 1);
        }
        return diff == 0; 
    }
    catch (e) 
    {
        console.log('ERROR: TimeConsistentEquals',e);
    } 
    return false;
} 

const genToken = (payload, subject) => { 
    const privateKey = fs.readFileSync(join(__dirname, './../private.key'));

    const token = jwt.sign(payload, process.env.JWT_SECRET, {
            expiresIn: 86400, // expires in 24 hours
            issuer: process.env.JWT_ISSUER,
            audience: process.env.JWT_AUDIENCE,
            subject: subject,
            encoding: 'utf8',
            keyid: subject,
            jwtid: uuidv4()
        }
    );
    return token;
}

const verifyToken = (token, cb) => {
    return jwt.verify(token, process.env.JWT_SECRET,{
        issuer: process.env.JWT_ISSUER,
        audience: process.env.JWT_AUDIENCE
    }, (error, decoded) => {       
        if(error)
            console.log(error); 
        cb(!error ? decoded : null);
    });
}

const requireTokenHandler = (req, res, next) => {
    if(req.headers.authorization){
        const token = req.headers.authorization.replace('Bearer ', '');
        return verifyToken(token, decoded => {
            if (decoded) {
                next();
            } else {
                res.locals.setBody({error: 'Authorization failed', status: 401});
                next(new Error("Authorization failed"));
            }
        });
    }else{
        res.locals.setBody({error: 'Authorization failed', status: 401});
        return next(new Error("Authorization failed"));
    }
}

module.exports = {
    hash,
    timeConsistentEquals,
    genToken,
    verifyToken,
    requireTokenHandler
}