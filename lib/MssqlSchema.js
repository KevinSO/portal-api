const constants = {
    SELECTALL : 'SELECTALL',
    SELECT : 'SELECT',
    UPDATE : 'UPDATE',
    DELETE : 'DELETE'
}
const types = {
    String: 'String',
    Integer: 'Integer'
}
const buildWhereStatement = (columns, filter = []) => {
    if(!Array.isArray(filter) || filter.length === 0)
        return '';

    let whereStatement = 'WHERE ';
    whereStatement += filter.map(condition => {
        const identifierType = columns.filter(col => col.name === condition.column).length > 0 ? columns.filter(col => col.name === condition.column)[0].type : types.String;
        return `[${condition.column}] = ${identifierType === types.String ? `'${condition.value}'` : condition.value}`;
    }).join(' AND ');
    
    return whereStatement;
}

const buildQuery = ({tableName, schema, identifier, columns} = {}, querytype = constants.SELECTALL, parameter = '', filter = []) => {
    switch(querytype){
        case constants.SELECT: {
            filter.push({column: identifier, value: parameter});
        }
        default:
        case constants.SELECTALL: {
            const whereStatement = buildWhereStatement(columns, filter);
            return `SELECT ${columns.map(col => `[${col.name}] as ${col.name.toLowerCase()}`).join(', ')} FROM [${schema}].[${tableName}] ${whereStatement}`;
        }
    }
}

class MssqlSchema {
    constructor({tableName = '', schema = 'dbo', identifier = 'id', columns = []}) {
        this.tableName = tableName;
        this.schema = schema;
        this.identifier = identifier;
        this.columns = columns;
    }

    async find() {
        const mssql = require('./mssql-query-handler');
        return await mssql.query(buildQuery(this, constants.SELECTALL), mssql.MULTIPLEROW);
    }

    async findOne(parameter){
        const mssql = require('./mssql-query-handler');
        const query = buildQuery(this, constants.SELECT, parameter);
        return await mssql.query(query, mssql.SINGLEROW, parameter);
    }

    async insertMany(arr){
        throw new Error("insertMany not implemented");
    }
    async updateAndSave(parameter, data){
        throw new Error("updateAndSave not implemented");
    }

    async deleteOne(parameter, data){
        throw new Error("deleteOne not implemented");
    }

}

class Column {
    constructor({name = '', type = types.String}) {
        this.name = name;
        this.type = type;
    }
}

module.exports = {
    MssqlSchema,
    Column,
    ColumnTypes : types,
    QueryConstants: constants,
    buildQuery
};