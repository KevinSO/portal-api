
 const mcache = require('memory-cache');
 const cache = (duration) => {
    return (req, res, next) => {
        let key = `__express__${req.originalUrl || req.url}`;
        let key_json = `${key}_json`;
        let cachedBody = mcache.get(key);
        let cachedBodyJson = mcache.get(key_json);
        if(cachedBody){
            console.log(`request took: ${(new Date()).getTime() - res.locals.reqTime}ms (Cache)`);
            res.send(cachedBody);
            return;
        }else if(cachedBodyJson){
            console.log(`request took: ${(new Date()).getTime() - res.locals.reqTime}ms (Cache)`);
            res.json(JSON.parse(cachedBodyJson));
            return;
        }else{
            res.sendResponse = res.send;
            res.send = (body) => {
                mcache.put(key, body, duration * 1000);
                res.sendResponse(body);
            }
            res.jsonResponse = res.json;
            res.json = (body) => {
                key = key_json;
                res.jsonResponse(body);
            }
            next();
        }
    }
 }

 module.exports = {
     cache,
     DEFAULT_LIFETIME : 60 * 60
};