const schemaInfo = require('../../lib/MssqlSchema');

const PortalLogin = new schemaInfo.MssqlSchema({
    tableName: 'PortalLogins', 
    schema: 'general', 
    identifier: 'EmailAddress',
    columns: [
        new schemaInfo.Column({name: 'Id', type: schemaInfo.ColumnTypes.Integer}),
        new schemaInfo.Column({name: 'AccountId', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'EmailAddress', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'Name', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'Password', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'ResetCode', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'ResetCodeExpires', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'Salt', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'NodePassword', type: schemaInfo.ColumnTypes.String}),
    ]
});


module.exports = PortalLogin;
