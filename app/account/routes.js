const AccountRouter = require('./Router');
const pack = require('./package');
const multer = require('multer')().none();

module.exports = router => {
    router.post(`${pack.route}/login`, multer, AccountRouter.login);
    router.post(`${pack.route}/admin-login`, multer, AccountRouter.adminLogin);
    router.post(`${pack.route}/register`, multer, AccountRouter.register);
    router.post(`${pack.route}/forgot-password`, multer, AccountRouter.forgotPassword);
    router.post(`${pack.route}/reset-password`, multer, AccountRouter.resetPassword);
    router.post(`${pack.route}/validate-code`, multer, AccountRouter.codeValid);
};
