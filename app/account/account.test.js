const mongoose = require('mongoose');

describe('Account', () => {
    const index = require('./index');
    const pack = require('./package');
    const accountSchema = require('./model');
    const Router = require('./Router');
    const routes = require('./routes');
    let Account;

    beforeAll(async () => {
        await mongoose.connect(`mongodb://${process.env.MONGO_URL}/${process.env.MONGO_DB}`);

        Account = mongoose.model(index.modelName, accountSchema);
    });

    // afterAll(() => mongoose.disconnect());


    describe('index', () => {

        test('Should have property model', () => {
            expect(index).toHaveProperty('model');
        });

        test('Should have property modelName with value Account', () => {
            expect(index).toHaveProperty('modelName', 'Account');
        });

        test('Should have property routes', () => {
            expect(index).toHaveProperty('routes');
        });
    });

    describe('package.json', () => {

        test('Should have property name with value account', () => {
            expect(pack).toHaveProperty('name', 'account');
        });

        test('Should have property route with value /account', () => {
            expect(pack).toHaveProperty('route', '/account');
        });

        test('Should have property main with value index', () => {
            expect(pack).toHaveProperty('main', 'index');
        });
    });

    describe('Router.js', () => {
        let req;
        let res;
        let next;
        let insertedAccount;
        const testAccounts = require('./account.test.json');

        beforeEach(async () => {
            req = {
                params: {}
            };
            res = {
                locals: {
                    body: null
                },
                status: jest.fn()
            };
            next = jest.fn();
            res.locals.setBody = function (body) {
                this.body = body;
            };

            insertedAccounts = await Account.insertMany(testAccounts);
        });

        afterEach(async () => {
            await Account.deleteMany({});
        });

        describe('.getModel', () => {
            test('Should return the Account model', () => {
                const result = Router.getModel();
                expect(result).toEqual(Account);
            });
        });

        describe('.get', () => {
            test('Should return a list of Account documents', async () => {
                await Router.get(req, res, next);

                expect(res.locals.body).toHaveProperty('account');
                expect(res.locals.body.account.length).toEqual(testAccounts.length);

                expect(next).toHaveBeenCalledTimes(1);
            });
        });

        describe('.getById', () => {
            test('Should return one requested Account document', async () => {
                req.params.id = insertedAccounts[0].id;
                await Router.getById(req, res, next);

                expect(res.locals.body).toHaveProperty('account');
                expect(res.locals.body.account).toBeInstanceOf(Object);
                expect(res.locals.body.account._id).toEqual(insertedAccounts[0]._id);
                expect(next).toHaveBeenCalledTimes(1);
            });
        });

        describe('.post', () => {
            test('Should set response status to 201', async () => {
                req.body = {};
                await Router.post(req, res, next);

                expect(res.status.mock.calls[0][0]).toBe(201);
                expect(next).toHaveBeenCalledTimes(1);
            });

            test('Should contain an object when only one new account is given', async () => {
                req.body = {};
                await Router.post(req, res, next);

                expect(res.locals.body.account).toBeInstanceOf(Object);
                expect(next).toHaveBeenCalledTimes(1);
            });

            test('Should contain an array when multiple accounts are given', async () => {
                req.body = [{}, {}];
                await Router.post(req, res, next);

                expect(Array.isArray(res.locals.body.account)).toBeTruthy();
                expect(next).toHaveBeenCalledTimes(1);
            });
        });

        describe('.put', () => {
            //TODO: Can be done after a schema is defined
        });

        describe('.delete', () => {
            test('Should delete the document with the given ID', async () => {
                const id = insertedAccounts[0]._id;
                req.params.id = id;

                await Router.delete(req, res, next);

                const result = await Account.findOne({_id: id});

                const resultArr = await Account.find();

                expect(result).toEqual(null);
                expect(resultArr.length).toEqual(insertedAccounts.length - 1);
                expect(next).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('routes.js', () => {
        let spyRouter;

        beforeEach(() => spyRouter = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
            delete: jest.fn()
        });

        test('Should be a function', () => {
            expect(routes).toBeInstanceOf(Function);
        });

        test('Should call param.get for GET route', () => {
            routes(spyRouter);
            expect(spyRouter.get.mock.calls[0][0]).toBe(pack.route);
            expect(spyRouter.get.mock.calls[0][1]).toBe(Router.get);
        });

        test('Should call param.get for GET id route', () => {
            routes(spyRouter);
            expect(spyRouter.get.mock.calls[1][0]).toBe(`${pack.route}/:id`);
            expect(spyRouter.get.mock.calls[1][1]).toBe(Router.getById);
        });

        test('Should call param.post for POST route', () => {
            routes(spyRouter);
            expect(spyRouter.post.mock.calls[0][0]).toBe(pack.route);
            expect(spyRouter.post.mock.calls[0][1]).toBe(Router.post);
        });

        test('Should call param.put for PUT id route', () => {
            routes(spyRouter);
            expect(spyRouter.put.mock.calls[0][0]).toBe(`${pack.route}/:id`);
            expect(spyRouter.put.mock.calls[0][1]).toBe(Router.put);
        });

        test('Should call param.delete for DELETE id route', () => {
            routes(spyRouter);
            expect(spyRouter.delete.mock.calls[0][0]).toBe(`${pack.route}/:id`);
            expect(spyRouter.delete.mock.calls[0][1]).toBe(Router.delete);
        });
    });

});