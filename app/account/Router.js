const _ = require("lodash");
const auth = require('../../lib/authentication');
const mssql = require('../../lib/mssql-query-handler');
const mailer = require('../../lib/mailer');
const uuidv4 = require('uuid/v4');
const moment = require('moment');
const passwordValidator = require('password-validator');
const emailValidator = require("email-validator");

class AccountRouter {

    static getModel () {
        return require('./model'); 
    }

    static async login(req, res, next) {
        const Account = AccountRouter.getModel();
        const result = await Account.findOne(req.body.user);
        if(result && result.id){
            const newHash = auth.hash(req.body.password + result.salt, result.salt);
            const matches = auth.timeConsistentEquals(result.nodepassword, newHash);
            if(matches){
                const query = "SELECT LocationId, OrganisationId FROM general.PortalLoginLocations " +
                              "INNER JOIN general.Locations ON PortalLoginLocations.LocationId = Locations.Id " +
                              "WHERE PortalLoginLocations.PortalLoginId = " + result.id;
                const locOrgs = await mssql.query(query, mssql.MULTIPLEROW);
                const locs = [];
                const orgs = [];
                locOrgs.forEach(locOrg => {
                    if(locs.indexOf(locOrg.LocationId) === -1)
                        locs.push(locOrg.LocationId);
                    if(orgs.indexOf(locOrg.OrganisationId) === -1 && locOrg.OrganisationId !== null)
                        orgs.push(locOrg.OrganisationId);
                });
                const token = auth.genToken(
                    { 
                        organisations: orgs,
                        locations: locs,
                        name: result.name
                    }, result.accountid);
                res.locals.setBody({token, status: 200});
                return next();
            }
        }
        res.locals.setBody({error: 'Authorization failed', status: 401});
        return next(new Error('Authorization failed'));
    }

    static async adminLogin(req, res, next) {
        if(req.body.password){
            const salt = '3A-BC-B6-FB-6D-F6-28-DF-02-96-17-9F-31-F0-18-CF';
            const hash = 'JpdRaRFmld7Vys3/2aan1oxwysTPY9KquMZsiPNF13UtZ/ZGnxYz99sDgsV8RBJ/Fn00ny8lt4LYBiubHgpWUQ==';
            const newHash = auth.hash(req.body.password + salt, salt);
            const matches = auth.timeConsistentEquals(hash, newHash);
            if(matches){
                const query = "SELECT Id FROM general.Locations WHERE Locations.OrganisationId = " + req.body.organisation;
                const locsResult = await mssql.query(query, mssql.MULTIPLEROW);
                const locs = [];
                locsResult.forEach(loc => {
                    if(locs.indexOf(loc.Id) === -1)
                        locs.push(loc.Id);
                });
                const token = auth.genToken(
                    { 
                        organisations: [req.body.organisation],
                        locations: locs,
                        name: "Slim Opgewekt"
                    }, uuidv4()
                );
                res.locals.setBody({token, status: 200});
                return next();
            }else{
                console.log('Hashes dont match', hash, newHash)
            }
        }
        res.locals.setBody({error: 'Authorization failed', status: 401});
        return next(new Error('Authorization failed'));
    }

    static async registerAccount(name, email, password, locations){
        try {
            const salt = AccountRouter.generateString();
            const hash = auth.hash(password + salt, salt);
            const accountId = uuidv4();
            const query = "INSERT INTO general.PortalLogins (AccountId, Name, EmailAddress, Salt, NodePassword) VALUES (@accountid, @name, @email, @salt, @hash);SELECT SCOPE_IDENTITY() as newId;";
            const accountResult = await mssql.queryWithParams(query, [
                {name: 'name', type: mssql.types.VarChar(64), value: name },
                {name: 'accountid', type: mssql.types.VarChar(128), value: accountId },
                {name: 'email', type: mssql.types.VarChar(256), value: email },
                {name: 'salt', type: mssql.types.VarChar(128), value: salt },
                {name: 'hash', type: mssql.types.VarChar(128), value: hash },
            ], mssql.SINGLEROW); 
            if(accountResult && accountResult.newId){
                const queryLocations = "INSERT INTO general.PortalLoginLocations (LocationId, PortalLoginId) VALUES " +
                    locations.map(loc => {
                        return `(${parseInt(loc)}, ${accountResult.newId})`;
                    }).join(', ');
                await mssql.query(queryLocations, mssql.NORESPONSE);
                return {status: 'success', message: `Account added with ID: ${accountResult.newId}`};
            }
        } catch(err) {
            return {status: 'error', message: err.message};
        }
        return {status: 'error', message: 'failed to add account'};
    }

    static validateRegistration(name, email, password, locations, res) {
        if(!name){
            res.send({status: 'error', message: 'Name is a required value'});
            return false;
        }

        const passValidator = new passwordValidator();
        let passValid = false;
        passValidator
            .is().min(8)
            .is().max(100)
            .has().uppercase()
            .has().lowercase()
            .has().digits();
        try {
            passValid = passValidator.validate(password);
        } catch(err) {

        }
        if(!passValid){
            res.send({status: 'error', message: 'Password must be atleast 8 characters and contain a digit, lowercase and uppercase character'});
            return false;
        }

        const emailValid = emailValidator.validate(email);
        if(!emailValid){
            res.send({status: 'error', message: 'Emailaddress not valid'});
            return false;
        }

        if(!locations || !Array.isArray(locations) || locations.length < 1){
            res.send({status: 'error', message: 'Locations are required'});
            return false;
        }
        return true;
    }


    static async register(req, res, next) {
        const name = req.body.name;
        const email = req.body.email;
        const password = req.body.password;
        const locations = req.body.locations;
        
        if(!AccountRouter.validateRegistration(name, email, password, locations, res))
            return;

        const response = await AccountRouter.registerAccount(name, email, password, locations);
        res.send(response);
        next();
    }

    static async forgotPassword(req, res, next) {
        console.log('hi!')
        const query = "SELECT Id as id, Name as name, AccountId as accountId, EmailAddress as email FROM general.PortalLogins WHERE EmailAddress = @email";
        const emailResult = await mssql.queryWithParams(query, [
            {name: 'email', type: mssql.types.VarChar(128), value: req.body.email }
        ], mssql.SINGLEROW); 
        console.log(emailResult);

        if(emailResult){
            const resetCode = AccountRouter.generateString();
            const queryUpdate = "UPDATE general.PortalLogins SET ResetCode = @resetCode, ResetCodeExpires = DATEADD(day, 1, GETDATE()) WHERE AccountId = @accountId";
            await mssql.queryWithParams(queryUpdate, [
                {name: 'resetCode', type: mssql.types.VarChar(128), value: resetCode },
                {name: 'accountId', type: mssql.types.VarChar(128), value: emailResult.accountId }
            ], mssql.NORESPONSE); 

            const emailContent = await mailer.buildTemplate('forgot-password', {
                name: emailResult.name, 
                code: resetCode,
                uri: `${process.env.PASS_RESET_URI}${resetCode}`
            });
            await mailer.sendEmail({
                emailTo: emailResult.email, 
                nameTo: emailResult.name, 
                subject: 'Jouw Opgewekt Portaal wachtwoord vervangen', 
                content: emailContent
            });
        }
        res.send({status: 'success'});
        next();
    }

    static async codeValid(req, res, next) {
        const query = "SELECT ResetCode as resetCode, ResetCodeExpires as resetCodeExpires FROM general.PortalLogins WHERE ResetCode = @resetCode";
        const emailResult = await mssql.queryWithParams(query, [
            {name: 'resetCode', type: mssql.types.VarChar(128), value: req.body.code }
        ], mssql.SINGLEROW); 
        if(emailResult && Object.keys(emailResult).length > 0){
            const expires = moment(emailResult.resetCodeExpires);
            if(expires.isAfter(moment())){
                res.send({status: 'valid'});
            }else{
            res .send({status: 'expired'});
            }
        }else{
            res.send({status: 'invalid'});
        }
        next();
    }

    static async resetPassword(req, res, next) {
        if(req.body.password && req.body.password2 && req.body.code){
            const passValidator = new passwordValidator();
            passValidator
                .is().min(8)
                .is().max(100)
                .has().uppercase()
                .has().lowercase()
                .has().digits();
            const passValid = passValidator.validate(req.body.password) 
                && req.body.password === req.body.password2;
            if(passValid){
                const salt = AccountRouter.generateString();
                const hash = auth.hash(req.body.password + salt, salt);
                const queryUpdate = "UPDATE general.PortalLogins SET Salt = @salt, NodePassword = @hash, ResetCode = NULL, ResetCodeExpires = NULL WHERE ResetCode = @resetCode";
                await mssql.queryWithParams(queryUpdate, [
                    {name: 'salt', type: mssql.types.VarChar(128), value: salt },
                    {name: 'hash', type: mssql.types.VarChar(256), value: hash },
                    {name: 'resetCode', type: mssql.types.VarChar(128), value: req.body.code }
                ], mssql.NORESPONSE); 
                res.send({status: 'success'});
            }else{
                res.send({status: 'invalid password'});
            }
        }else{
            res.send({status: 'missing code'});
        }
        next();
    }

    static generateString() {
        let salt = [];
        for(let i = 0; i < 16; i++){
            salt.push(_.random(0,255));
        }
        const saltStr = salt.map(num => (num < 16 ? '0' : '') + num.toString(16).toUpperCase()).join('-');

        return saltStr;
        //res.send({saltStr, hash: auth.hash('a9Q%uhFB^1GviWf4' + saltStr, saltStr)});
    }
}

module.exports = AccountRouter;
