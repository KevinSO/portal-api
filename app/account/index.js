module.exports = {
    modelName: 'Account',
    model: require('./model'),
    routes: require('./routes')
};

