const LocationRouter = require('./Router');
const {requireTokenHandler} = require('../../lib/authentication');
const {cache, DEFAULT_LIFETIME} = require('../../lib/caching');
module.exports = router => {
    router.get(`/organisation/:org/locations`, requireTokenHandler, cache(DEFAULT_LIFETIME), LocationRouter.get);
    router.get(`/organisation/:org/locations/:id`, requireTokenHandler, cache(DEFAULT_LIFETIME), LocationRouter.getById);
    router.get(`/organisation/:org/locations/:id/data/years`, requireTokenHandler, cache(DEFAULT_LIFETIME), LocationRouter.getYears);
    router.get(`/organisation/:org/locations/:id/data/months/:start`, requireTokenHandler, cache(DEFAULT_LIFETIME), LocationRouter.getMonths);
    router.get(`/organisation/:org/locations/:id/data/prognosis/:year`, cache(DEFAULT_LIFETIME), LocationRouter.getPrognosis);
    router.get(`/organisation/:org/locations/:id/systems/`, requireTokenHandler, cache(DEFAULT_LIFETIME), LocationRouter.listSystems);
    router.get(`/organisation/:org/locations/:id/systems/:type/:sys`, requireTokenHandler, cache(DEFAULT_LIFETIME), LocationRouter.getSystem);
};
