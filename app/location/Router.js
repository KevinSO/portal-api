class LocationRouter {

    static getModel () {
        return require('./model'); 
    }

    static async get(req, res, next) {
        const Location = LocationRouter.getModel();
        const result = await Location.list(req.params.org);
        res.locals.setBody({id: parseInt(req.params.id), result: result});
        next();
    }

    static async getById(req, res, next) {
        const Location = LocationRouter.getModel();
        const result = await Location.getLocation(req.params.org,req.params.id);
        res.locals.setBody({id: parseInt(req.params.id), result: result});
        next();
    }

    static async getYears(req, res, next) {
        const Location = LocationRouter.getModel();
        const yieldData = await Location.getYieldYears(req.params.org,req.params.id);
        const consumptionData = await Location.getConsumptionYears(req.params.org,req.params.id);
        res.locals.setBody({id: parseInt(req.params.id), yield: yieldData, consumption: consumptionData});
        next();
    }

    static async getMonths(req, res, next) {
        const Location = LocationRouter.getModel();
        const yieldData = await Location.getYieldMonths(req.params.org,req.params.id, req.params.start);
        const consumptionData = await Location.getConsumptionMonths(req.params.org,req.params.id, req.params.start);
        res.locals.setBody({id: parseInt(req.params.id), yield: yieldData, consumption: consumptionData});
        next();
    }

    static async getPrognosis(req, res, next) {
        const Location = LocationRouter.getModel();
        const result = await Location.getPrognosis(req.params.org,req.params.id, req.params.year);
        res.locals.setBody({id: parseInt(req.params.id), result: result});
        next();
    }

    static async listSystems(req, res, next) {
        const Location = LocationRouter.getModel();
        const result = await Location.listSystems(req.params.org, req.params.id);
        const locationInfo = await Location.getLocationInfo(req.params.org, req.params.id);
        res.locals.setBody({id: parseInt(req.params.id), systems: result, location: locationInfo});
        next();
    }

    static async getSystem(req, res, next) {
        const Location = LocationRouter.getModel();
        const result = await Location.getSystem(req.params.org, req.params.id, req.params.type, req.params.sys);
        res.locals.setBody({id: parseInt(req.params.id), type: req.params.type ,system: result});
        next();
    }
    
}



module.exports = LocationRouter;
