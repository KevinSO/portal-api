const schemaInfo = require('../../lib/MssqlSchema');
const mssql = require('../../lib/mssql-query-handler');

const Location = new schemaInfo.MssqlSchema({
    tableName: 'Locations', 
    schema: 'general', 
    identifier: 'Id',
    columns: [
        new schemaInfo.Column({name: 'Id', type: schemaInfo.ColumnTypes.Integer}),
        new schemaInfo.Column({name: 'Name', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'AddressId', type: schemaInfo.ColumnTypes.Integer}),
        new schemaInfo.Column({name: 'OrganisationId', type: schemaInfo.ColumnTypes.Integer}),
        new schemaInfo.Column({name: 'CompletionDate', type: schemaInfo.ColumnTypes.String}),
    ]
});

const moment = require('moment');
const _ = require('lodash');
const {
    mergeInverterData,
    calculateIntervalsYield,
    groupConsumptionData
} = require('../../lib/energyprocessing');

const asvzSpecialCasesNaming = (row) => {
    const names = {
        177: 'Muziektent',
        181: 'Tuincentrum',
        182: 'Kinderboerderij',
        197: 'Parkeeroverkapping',
        204: 'Zwembad'
    };
    if([197, 177, 181, 182, 204].indexOf(row.id) > -1)
        row.name += ` ${names[row.id]}`;
}

Location.list = async (org) => {
    if(org != 14)
        return await mssql.query(schemaInfo.buildQuery(Location, schemaInfo.QueryConstants.SELECTALL, '', [{column: 'OrganisationId', value: org}]), mssql.MULTIPLEROW);
    else { //asvz
        
    const query = `
        SELECT 
            Locations.Id as id, 
            Cities.Name + ', ' + Addresses.Street + ' ' + Addresses.Number  as name, 
            Locations.Name as originalName,
            Cities.Name as cityName,
            OrganisationId as organisationid, 
            CompletionDate as completiondate
        FROM general.Locations INNER JOIN general.Addresses ON Locations.AddressId = Addresses.Id INNER JOIN general.Cities ON Addresses.CityId = Cities.Id
        WHERE OrganisationId = @organisationId
    `;
    const result =  await mssql.queryWithParams(query, [{name: 'organisationId', type: mssql.types.Int , value: org}], mssql.MULTIPLEROW);
    result.forEach(asvzSpecialCasesNaming);
    return result;
    }
}
Location.getLocation = async (org, loc) => {
    
    if(org != 14) {
        const query = `
            SELECT 
                Id as id, 
                Name as name, 
                AddressId as addressid, 
                OrganisationId as organisationid, 
                CompletionDate as completiondate,
                ISNULL((SELECT TOP 1 1 FROM logging.Inverters WHERE LocationId = Locations.Id AND LastETotal > 0),0) as hasyield,
                ISNULL((SELECT TOP 1 1 FROM logging.ConsumptionMeters WHERE LocationId = Locations.Id AND LastUsage > 0),0) as hasconsumption
            FROM general.Locations 
            WHERE id = @id AND OrganisationId = @organisationId
        `;
        return await mssql.queryWithParams(query, [{name: 'id', type: mssql.types.Int , value: loc},{name: 'organisationId', type: mssql.types.Int , value: org}], mssql.SINGLEROW);
    }else{
        const query = `
            SELECT 
                Locations.Id as id, 
                Cities.Name + ', ' + Addresses.Street + ' ' + Addresses.Number  as name, 
                Locations.Name as originalName,
                Cities.Name as cityName, 
                AddressId as addressid, 
                OrganisationId as organisationid, 
                CompletionDate as completiondate,
                ISNULL((SELECT TOP 1 1 FROM logging.Inverters WHERE LocationId = Locations.Id AND LastETotal > 0),0) as hasyield,
                ISNULL((SELECT TOP 1 1 FROM logging.ConsumptionMeters WHERE LocationId = Locations.Id AND LastUsage > 0),0) as hasconsumption
            FROM general.Locations INNER JOIN general.Addresses ON Locations.AddressId = Addresses.Id INNER JOIN general.Cities ON Addresses.CityId = Cities.Id
            WHERE Locations.id = @id AND OrganisationId = @organisationId
        `;
        const result = await mssql.queryWithParams(query, [{name: 'id', type: mssql.types.Int , value: loc},{name: 'organisationId', type: mssql.types.Int , value: org}], mssql.SINGLEROW);
        asvzSpecialCasesNaming(result);
        return result;
    }
} 

Location.getYieldYears = async (org, loc) => {
    const mergedData = mergeInverterData(await mssql.query(`EXEC logging.GetLocYearTotalYieldLogs ${loc}, '${'2017-01-01'}', '${'2018-01-01'}'`, mssql.MULTIPLEROW), true);
    return calculateIntervalsYield(mergedData, moment('2017-01-01'), true);
}
Location.getYieldMonths = async (org, loc, start) => {
    const startMoment = moment(start).startOf('month');
    const momentKeys = [];
    for(let i = -1; i < 12; i++)
        momentKeys.push(startMoment.clone().add(i, 'month').format('YYYY-MM'));
    const mergedData = mergeInverterData(await mssql.query(`EXEC logging.GetLocMonthTotalYieldLogs ${loc}, '${startMoment.format('YYYY-MM-DD')}', '${startMoment.clone().add(1, 'year').format('YYYY-MM-DD')}'`, mssql.MULTIPLEROW), false, momentKeys);
    return calculateIntervalsYield(mergedData, startMoment);
}
Location.getConsumptionYears = async (org, loc) => {
    const data = await mssql.query(`EXEC logging.GetLocYearTotalConsumptionLogs ${loc}, '${'2017-01-01'}', '${'2040-01-01'}'`, mssql.MULTIPLEROW);
    return groupConsumptionData(data, moment('2017-01-01'), 'YYYY', true);
}
Location.getConsumptionMonths = async (org, loc, start) => {
    const startMoment = moment(start).startOf('month');
    const data = await mssql.query(`EXEC logging.GetLocMonthTotalConsumptionLogs ${loc}, '${startMoment.format('YYYY-MM-DD')}', '${startMoment.clone().add(1, 'year').format('YYYY-MM-DD')}'`, mssql.MULTIPLEROW);
    return groupConsumptionData(data, startMoment, 'YYYY-MM');
}
Location.getPrognosis = async (org, loc, year) => {
    
    if(org.toString() === '15')
        return {};
        
    const sustainabilityData = await mssql.query(`SELECT UsageBefore, LedReduction FROM kiosk.SustainabilityResults WHERE LocationId = ${loc}`, mssql.SINGLEROW);
    const inverterData = await mssql.query(`SELECT ROUND(SUM(Panels * PanelWP * YieldFactor * ((1 - DATEDIFF(year, CompletionDate, '${year}') * 0.005))),0) as val FROM logging.Inverters INNER JOIN general.Locations ON general.Locations.Id = logging.Inverters.LocationId WHERE logging.Inverters.InUse = 1 AND general.Locations.Id = ${loc}`, mssql.SINGLEROW);
    
    const result = {};
    
    if(sustainabilityData && sustainabilityData.UsageBefore && sustainabilityData.LedReduction){
        result.netUsageExpected = sustainabilityData.UsageBefore - sustainabilityData.LedReduction;
    }
    
    if(inverterData && inverterData.val){
        const monthPercentages = [
            0.026,
            0.044,
            0.086,
            0.119,
            0.134,
            0.131,
            0.130,
            0.116,
            0.094,
            0.069,
            0.032,
            0.019
        ];
        result.yieldExpected = monthPercentages.map((perc, index) => {
            return {
                time: moment(new Date(year, index)).format('YYYY-MM'),
                yield: _.round(inverterData.val * perc,0)
            };
        })

    }

    return result;
}

Location.listSystems = async (org, loc) => {
    const unionQuery = `SELECT 'inverter' as type, Id as id, Size as description, InUse as active FROM logging.Inverters WHERE LocationId = ${loc} ` +
                       `UNION ` +
                       `SELECT 'meter', Id, CONCAT(CASE WHEN Type = 3 THEN 'Tussenmeter' ELSE 'Slimme meter' END, ' ', Description), 1 FROM logging.ConsumptionMeters WHERE LocationId = ${loc}`;
    const result =  await mssql.query(unionQuery, mssql.MULTIPLEROW);
    return result;
}

Location.getLocationInfo = async (org, loc) => {
    const query = "SELECT ISNULL(SUM(Panels * PanelWP),0) as wp, ISNULL(SUM(Panels),0) as panels, Name as name FROM general.Locations " + 
                  "LEFT OUTER JOIN logging.Inverters ON Locations.Id = Inverters.LocationId " +
                  `WHERE Locations.Id = ${loc} AND Inverters.InUse = 1` +
                  "GROUP BY Locations.Id, Locations.Name";
    return await mssql.query(query, mssql.SINGLEROW);
}

Location.getSystem = async (org, loc, type, id) => {
    if(type === 'inverter'){
        const query = `
        SELECT DATEPART(Year, Time) as year, DATEPART(Month, Time) as month, ROUND(MAX(Etotal),1) as endvalue
        FROM logging.YieldLogs WHERE InverterId = ${id}
        
        GROUP BY DATEPART(Year, Time), DATEPART(Month, Time)
        ORDER BY year, month
        `;
        const data = await mssql.query(query, mssql.MULTIPLEROW);
        return data.map(row => {
            return {
                date: `${row.year}-${row.month <= 9 ? '0' : ''}${row.month}`,
                value: row.endvalue
            }
        });
    }else if(type === 'meter'){
        const query = `
            SELECT DATEPART(Year, Time) as year, DATEPART(Month, Time) as month, 
            ROUND(MAX(EtotalInHigh),0) as inHigh, ROUND(MAX(EtotalInLow),0) as inLow, 
            ROUND(MAX(EtotalOutHigh),0) as outHigh, ROUND(MAX(EtotalOutLow),0) as outLow
            FROM logging.ConsumptionLogs WHERE ConsumptionMeterId = ${id}
            
            GROUP BY DATEPART(Year, Time), DATEPART(Month, Time)
            ORDER BY year, month
        `;
        const data = await mssql.query(query, mssql.MULTIPLEROW);
        return data.map(row => {
            return {
                date: `${row.year}-${row.month <= 9 ? '0' : ''}${row.month}`,
                valueInHigh: row.inHigh,
                valueInLow: row.inLow,
                valueOutHigh: row.outHigh,
                valueOutLow: row.outLow,
            }
        });
    }
}
module.exports = Location;

