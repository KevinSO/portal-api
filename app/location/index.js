module.exports = {
    modelName: 'Location',
    model: require('./model'),
    routes: require('./routes')
};

