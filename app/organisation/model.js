const schemaInfo = require('../../lib/MssqlSchema');
const mssql = require('../../lib/mssql-query-handler');

const Organisation = new schemaInfo.MssqlSchema({
    tableName: 'Organisations', 
    schema: 'general', 
    identifier: 'Id',
    columns: [
        new schemaInfo.Column({name: 'Id', type: schemaInfo.ColumnTypes.Integer}),
        new schemaInfo.Column({name: 'Name', type: schemaInfo.ColumnTypes.String}),
        new schemaInfo.Column({name: 'AddressId', type: schemaInfo.ColumnTypes.Integer}),
        new schemaInfo.Column({name: 'CustomerNr', type: schemaInfo.ColumnTypes.String}),
    ]
});

const moment = require('moment');
const _ = require('lodash');
const {
    mergeInverterData,
    calculateIntervalsYield,
    groupConsumptionData
} = require('../../lib/energyprocessing');


Organisation.getYieldYears = async (org) => {
    const mergedData = mergeInverterData(await mssql.query(`EXEC logging.GetOrgYearTotalYieldLogs ${org}, '${'2017-01-01'}', '${'2018-01-01'}'`, mssql.MULTIPLEROW), true);
    return calculateIntervalsYield(mergedData, moment('2017-01-01'), true);
}
Organisation.getYieldMonths = async (org, start) => {
    const startMoment = moment(start).startOf('month');
    const momentKeys = [];
    for(let i = -1; i < 12; i++)
        momentKeys.push(startMoment.clone().add(i, 'month').format('YYYY-MM'));
    const mergedData = mergeInverterData(await mssql.query(`EXEC logging.GetOrgMonthTotalYieldLogs ${org}, '${startMoment.format('YYYY-MM-DD')}', '${startMoment.clone().add(1, 'year').format('YYYY-MM-DD')}'`, mssql.MULTIPLEROW), false, momentKeys);
    return calculateIntervalsYield(mergedData, startMoment);
}
Organisation.getConsumptionYears = async (org) => {
    const data = await mssql.query(`EXEC logging.GetOrgYearTotalConsumptionLogs ${org}, '${'2017-01-01'}', '${'2040-01-01'}'`, mssql.MULTIPLEROW);
    return groupConsumptionData(data, moment('2017-01-01'), 'YYYY', true);
}
Organisation.getConsumptionMonths = async (org, start) => {
    const startMoment = moment(start).startOf('month');
    const data = await mssql.query(`EXEC logging.GetOrgMonthTotalConsumptionLogs ${org}, '${startMoment.format('YYYY-MM-DD')}', '${startMoment.clone().add(1, 'year').format('YYYY-MM-DD')}'`, mssql.MULTIPLEROW);
    return groupConsumptionData(data, startMoment, 'YYYY-MM');
}
Organisation.getPrognosis = async (org, year) => {
    if(org.toString() === '15')
        return {};

    const sustainabilityData = await mssql.query(`SELECT SUM(UsageBefore) as UsageBefore, SUM(LedReduction) as LedReduction FROM kiosk.SustainabilityResults INNER JOIN general.Locations ON general.Locations.Id = kiosk.SustainabilityResults.LocationId WHERE OrganisationId = ${org}`, mssql.SINGLEROW);
    const inverterData = await mssql.query(`SELECT ROUND(SUM(Panels * PanelWP * YieldFactor * ((1 - DATEDIFF(year, CompletionDate, '${year}') * 0.005))),0) as val FROM logging.Inverters INNER JOIN general.Locations ON general.Locations.Id = logging.Inverters.LocationId WHERE logging.Inverters.InUse = 1 AND general.Locations.OrganisationId = ${org}`, mssql.SINGLEROW);
    
    const result = {};
    
    if(sustainabilityData && sustainabilityData.UsageBefore && sustainabilityData.LedReduction){
        result.netUsageExpected = sustainabilityData.UsageBefore - sustainabilityData.LedReduction;
    }
    
    if(inverterData && inverterData.val){
        const monthPercentages = [
            0.026,
            0.044,
            0.086,
            0.119,
            0.134,
            0.131,
            0.130,
            0.116,
            0.094,
            0.069,
            0.032,
            0.019
        ];
        result.yieldExpected = monthPercentages.map((perc, index) => {
            return {
                time: moment(new Date(year, index)).format('YYYY-MM'),
                yield: _.round(inverterData.val * perc,0)
            };
        })

    }

    return result;
}
Organisation.getTotalYield = async (org) => {
    const data = await mssql.query(`SELECT Organisations.Id, Organisations.Name, ROUND(SUM(LastEtotal),0) as total FROM 
        general.Organisations
        INNER JOIN general.Locations ON Locations.OrganisationId = Organisations.Id
        INNER JOIN logging.Inverters ON Inverters.LocationId = Locations.Id
    WHERE Organisations.Id = ${org}
    GROUP BY Organisations.Id, Organisations.Name`, mssql.SINGLEROW);
    return data;
}

module.exports = Organisation;
