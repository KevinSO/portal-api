module.exports = {
    modelName: 'Organisation',
    model: require('./model'),
    routes: require('./routes')
};

