const mssql = require('../../lib/mssql-query-handler');

class OrganisationRouter {

    static getModel () {
        return require('./model'); 
    }

    static async list(req, res, next) {
        const Organisation = OrganisationRouter.getModel();
        const result = await Organisation.find();
        res.locals.setBody({result: result.map(org => {return {name: org.name, id: org.id}})});
        next();
    }

    static async get(req, res, next) {
        const Organisation = OrganisationRouter.getModel();
        const result = await Organisation.find();
        res.locals.setBody({result: result});
        next();
    }

    static async getById(req, res, next) {
        const Organisation = OrganisationRouter.getModel();
        const result = await Organisation.findOne(req.params.id);
        const query = "SELECT Type as type from general.Locations WHERE OrganisationId = " + req.params.id;
        const types = await mssql.query(query, mssql.MULTIPLEROW);
        const type = types.reduce((prev, row) => {
            if(prev === 999)
                return row.type;
            if(prev === row.type)
                return prev;
            return -1;
        }, 999);
        switch(type){
            default:
            case -1:
                result.type = 'so';
                break;
            case 0:
            case 1:
                result.type = 'sos';
                break;
            case 2:
                result.type = 'spo';
                break;
            case 3:
                result.type = 'ozo';
                break;
        }
        res.locals.setBody({id: parseInt(req.params.id), result: result});
        next();
    }

    static async getYears(req, res, next) {
        const Organisation = OrganisationRouter.getModel();
        const yieldData = await Organisation.getYieldYears(req.params.id);
        const consumptionData = await Organisation.getConsumptionYears(req.params.id);
        res.locals.setBody({id: parseInt(req.params.id), yield: yieldData, consumption: consumptionData});
        next();
    }

    static async getMonths(req, res, next) {
        const Organisation = OrganisationRouter.getModel();
        const yieldData = await Organisation.getYieldMonths(req.params.id, req.params.start);
        const consumptionData = await Organisation.getConsumptionMonths(req.params.id, req.params.start);
        res.locals.setBody({id: parseInt(req.params.id), yield: yieldData, consumption: consumptionData});
        next();
    }

    static async getPrognosis(req, res, next) {
        const Organisation = OrganisationRouter.getModel();
        const result = await Organisation.getPrognosis(req.params.id, req.params.year);
        res.locals.setBody({id: parseInt(req.params.id), result: result});
        next();
    }

    static async getTotalYield(req, res, next) {
        const Organisation = OrganisationRouter.getModel();
        const result = await Organisation.getTotalYield(req.params.id);
        res.locals.setBody({id: parseInt(req.params.id), result: result});
        next();
    }
}

module.exports = OrganisationRouter;
