const mongoose = require('mongoose');

describe('Organisation', () => {
    const index = require('./index');
    const pack = require('./package');
    const organisationSchema = require('./model');
    const Router = require('./Router');
    const routes = require('./routes');
    let Organisation;

    beforeAll(async () => {
        await mongoose.connect(`mongodb://${process.env.MONGO_URL}/${process.env.MONGO_DB}`);

        Organisation = mongoose.model(index.modelName, organisationSchema);
    });

    // afterAll(() => mongoose.disconnect());


    describe('index', () => {

        test('Should have property model', () => {
            expect(index).toHaveProperty('model');
        });

        test('Should have property modelName with value Organisation', () => {
            expect(index).toHaveProperty('modelName', 'Organisation');
        });

        test('Should have property routes', () => {
            expect(index).toHaveProperty('routes');
        });
    });

    describe('package.json', () => {

        test('Should have property name with value organisation', () => {
            expect(pack).toHaveProperty('name', 'organisation');
        });

        test('Should have property route with value /organisation', () => {
            expect(pack).toHaveProperty('route', '/organisation');
        });

        test('Should have property main with value index', () => {
            expect(pack).toHaveProperty('main', 'index');
        });
    });

    describe('Router.js', () => {
        let req;
        let res;
        let next;
        let insertedOrganisation;
        const testOrganisations = require('./organisation.test.json');

        beforeEach(async () => {
            req = {
                params: {}
            };
            res = {
                locals: {
                    body: null
                },
                status: jest.fn()
            };
            next = jest.fn();
            res.locals.setBody = function (body) {
                this.body = body;
            };

            insertedOrganisations = await Organisation.insertMany(testOrganisations);
        });

        afterEach(async () => {
            await Organisation.deleteMany({});
        });

        describe('.getModel', () => {
            test('Should return the Organisation model', () => {
                const result = Router.getModel();
                expect(result).toEqual(Organisation);
            });
        });

        describe('.get', () => {
            test('Should return a list of Organisation documents', async () => {
                await Router.get(req, res, next);

                expect(res.locals.body).toHaveProperty('organisation');
                expect(res.locals.body.organisation.length).toEqual(testOrganisations.length);

                expect(next).toHaveBeenCalledTimes(1);
            });
        });

        describe('.getById', () => {
            test('Should return one requested Organisation document', async () => {
                req.params.id = insertedOrganisations[0].id;
                await Router.getById(req, res, next);

                expect(res.locals.body).toHaveProperty('organisation');
                expect(res.locals.body.organisation).toBeInstanceOf(Object);
                expect(res.locals.body.organisation._id).toEqual(insertedOrganisations[0]._id);
                expect(next).toHaveBeenCalledTimes(1);
            });
        });

        describe('.post', () => {
            test('Should set response status to 201', async () => {
                req.body = {};
                await Router.post(req, res, next);

                expect(res.status.mock.calls[0][0]).toBe(201);
                expect(next).toHaveBeenCalledTimes(1);
            });

            test('Should contain an object when only one new organisation is given', async () => {
                req.body = {};
                await Router.post(req, res, next);

                expect(res.locals.body.organisation).toBeInstanceOf(Object);
                expect(next).toHaveBeenCalledTimes(1);
            });

            test('Should contain an array when multiple organisations are given', async () => {
                req.body = [{}, {}];
                await Router.post(req, res, next);

                expect(Array.isArray(res.locals.body.organisation)).toBeTruthy();
                expect(next).toHaveBeenCalledTimes(1);
            });
        });

        describe('.put', () => {
            //TODO: Can be done after a schema is defined
        });

        describe('.delete', () => {
            test('Should delete the document with the given ID', async () => {
                const id = insertedOrganisations[0]._id;
                req.params.id = id;

                await Router.delete(req, res, next);

                const result = await Organisation.findOne({_id: id});

                const resultArr = await Organisation.find();

                expect(result).toEqual(null);
                expect(resultArr.length).toEqual(insertedOrganisations.length - 1);
                expect(next).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('routes.js', () => {
        let spyRouter;

        beforeEach(() => spyRouter = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
            delete: jest.fn()
        });

        test('Should be a function', () => {
            expect(routes).toBeInstanceOf(Function);
        });

        test('Should call param.get for GET route', () => {
            routes(spyRouter);
            expect(spyRouter.get.mock.calls[0][0]).toBe(pack.route);
            expect(spyRouter.get.mock.calls[0][1]).toBe(Router.get);
        });

        test('Should call param.get for GET id route', () => {
            routes(spyRouter);
            expect(spyRouter.get.mock.calls[1][0]).toBe(`${pack.route}/:id`);
            expect(spyRouter.get.mock.calls[1][1]).toBe(Router.getById);
        });

        test('Should call param.post for POST route', () => {
            routes(spyRouter);
            expect(spyRouter.post.mock.calls[0][0]).toBe(pack.route);
            expect(spyRouter.post.mock.calls[0][1]).toBe(Router.post);
        });

        test('Should call param.put for PUT id route', () => {
            routes(spyRouter);
            expect(spyRouter.put.mock.calls[0][0]).toBe(`${pack.route}/:id`);
            expect(spyRouter.put.mock.calls[0][1]).toBe(Router.put);
        });

        test('Should call param.delete for DELETE id route', () => {
            routes(spyRouter);
            expect(spyRouter.delete.mock.calls[0][0]).toBe(`${pack.route}/:id`);
            expect(spyRouter.delete.mock.calls[0][1]).toBe(Router.delete);
        });
    });

});