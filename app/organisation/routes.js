const OrganisationRouter = require('./Router');
const pack = require('./package');
const {requireTokenHandler} = require('../../lib/authentication');
const {cache, DEFAULT_LIFETIME} = require('../../lib/caching');

module.exports = router => {
    router.get(`${pack.route}-list`, cache(DEFAULT_LIFETIME), OrganisationRouter.list);
    router.get(pack.route, requireTokenHandler, cache(DEFAULT_LIFETIME), OrganisationRouter.get);
    router.get(`${pack.route}/:id`, requireTokenHandler, cache(DEFAULT_LIFETIME), OrganisationRouter.getById);
    router.get(`${pack.route}/:id/data/years`, requireTokenHandler, cache(DEFAULT_LIFETIME), OrganisationRouter.getYears);
    router.get(`${pack.route}/:id/data/months/:start`, requireTokenHandler, cache(DEFAULT_LIFETIME), OrganisationRouter.getMonths);
    router.get(`${pack.route}/:id/data/prognosis/:year`, requireTokenHandler, cache(DEFAULT_LIFETIME), OrganisationRouter.getPrognosis);
    router.get(`${pack.route}/:id/total-yield`, requireTokenHandler, cache(DEFAULT_LIFETIME), OrganisationRouter.getTotalYield);
};
